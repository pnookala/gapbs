OMP_NUM_THREADS	1
Read Time:           8.14231
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.25631
   td          4     0.00002
   td         42     0.00001
   td       7681     0.00032
   td    9522739     0.42599
   td   19741889    10.95025
   td    5137920    10.76314
   td     546423     1.95616
   td      53310     0.19335
   td       5386     0.01923
   td        630     0.00193
   td         83     0.00021
   td         21     0.00003
   td          6     0.00001
   td          2     0.00000
   td          1     0.00000
   td          0     0.00000
Trial Time:          24.68187
Average Time:        24.68187
OMP_NUM_THREADS	2
Read Time:           7.27949
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.12813
   td          4     0.00003
   td         42     0.00002
   td       7681     0.00021
   td    9522739     0.28541
   td   19741889    12.06358
   td    5137920    13.88632
   td     546423     3.05291
   td      53310     0.30201
   td       5386     0.03411
   td        630     0.00342
   td         83     0.00039
   td         21     0.00005
   td          6     0.00002
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          29.83207
Average Time:        29.83207
OMP_NUM_THREADS	4
Read Time:           7.13101
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.06928
   td          4     0.00003
   td         42     0.00004
   td       7681     0.00017
   td    9522739     0.18042
   td   19741889    11.24740
   td    5137920    13.51565
   td     546423     3.26997
   td      53310     0.30609
   td       5386     0.03681
   td        630     0.00371
   td         83     0.00041
   td         21     0.00006
   td          6     0.00002
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          28.67937
Average Time:        28.67937
OMP_NUM_THREADS	8
Read Time:           7.07407
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.04223
   td          4     0.00003
   td         42     0.00006
   td       7681     0.00016
   td    9522739     0.11788
   td   19741889     9.80356
   td    5137920    19.04191
   td     546423     5.23264
   td      53310     0.39925
   td       5386     0.05550
   td        630     0.00524
   td         83     0.00055
   td         21     0.00009
   td          6     0.00003
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          34.73478
Average Time:        34.73478
OMP_NUM_THREADS	16
Read Time:           7.01656
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02579
   td          4     0.00004
   td         42     0.00004
   td       7681     0.00031
   td    9522739     0.05414
   td   19741889    12.62311
   td    5137920    28.04813
   td     546423     8.34812
   td      53310     0.75798
   td       5386     0.08892
   td        630     0.00945
   td         83     0.00102
   td         21     0.00017
   td          6     0.00006
   td          2     0.00003
   td          1     0.00001
   td          0     0.00000
Trial Time:          49.98518
Average Time:        49.98518
OMP_NUM_THREADS	24
Read Time:           7.01940
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02059
   td          4     0.00004
   td         42     0.00007
   td       7681     0.00040
   td    9522739     0.04241
   td   19741889    17.21225
   td    5137920    38.51144
   td     546423     9.95069
   td      53310     0.99533
   td       5386     0.11932
   td        630     0.01200
   td         83     0.00158
   td         21     0.00026
   td          6     0.00011
   td          2     0.00006
   td          1     0.00001
   td          0     0.00001
Trial Time:          66.89315
Average Time:        66.89315
OMP_NUM_THREADS	48
Read Time:           7.08216
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02123
   td          4     0.00005
   td         42     0.00016
   td       7681     0.00097
   td    9522739     0.07869
   td   19741889    46.20204
   td    5137920    91.83000
   td     546423    24.31125
   td      53310     2.42482
   td       5386     0.25970
   td        630     0.03020
   td         83     0.00397
   td         21     0.00103
   td          6     0.00054
   td          2     0.00020
   td          1     0.00002
   td          0     0.00003
Trial Time:          165.19472
Average Time:        165.19472
OMP_NUM_THREADS	96
Read Time:           7.32887
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02565
   td          4     0.00009
   td         42     0.00031
   td       7681     0.00266
   td    9522739     0.09419
   td   19741889    90.15238
   td    5137920   188.33233
   td     546423    49.01913
   td      53310     4.53959
   td       5386     0.52287
   td        630     0.05635
   td         83     0.00799
   td         21     0.00295
   td          6     0.00192
   td          2     0.00076
   td          1     0.00006
   td          0     0.00006
Trial Time:          332.79212
Average Time:        332.79212
OMP_NUM_THREADS	192
Read Time:           7.38916
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02302
   td          4     0.00014
   td         42     0.00057
   td       7681     0.00676
   td    9522739     0.13636
   td   19741889   119.34415
   td    5137920   234.88489
   td     546423    60.00251
   td      53310     5.82620
   td       5386     0.68654
   td        630     0.08243
   td         83     0.01220
   td         21     0.00644
   td          6     0.00368
   td          2     0.00155
   td          1     0.00009
   td          0     0.00009
Trial Time:          421.13080
Average Time:        421.13080
