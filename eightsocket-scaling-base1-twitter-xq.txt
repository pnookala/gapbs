OMP_NUM_THREADS	1
Read Time:           26.14993
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.23871
   td          4     0.00001
   td         42     0.00002
   td       7681     0.00031
   td    9522739     0.43323
   td   19741889    10.90112
   td    5137920    10.86610
   td     546423     2.01781
   td      53310     0.18652
   td       5386     0.02016
   td        630     0.00203
   td         83     0.00023
   td         21     0.00003
   td          6     0.00001
   td          2     0.00000
   td          1     0.00000
   td          0     0.00000
Trial Time:          24.78067
Average Time:        24.78067
OMP_NUM_THREADS	2
Read Time:           7.51963
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.12684
   td          4     0.00001
   td         42     0.00003
   td       7681     0.00023
   td    9522739     0.32766
   td   19741889     8.35933
   td    5137920     9.00847
   td     546423     2.03135
   td      53310     0.21257
   td       5386     0.02432
   td        630     0.00262
   td         83     0.00031
   td         21     0.00005
   td          6     0.00002
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          20.16823
Average Time:        20.16823
OMP_NUM_THREADS	4
Read Time:           7.07612
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.06884
   td          4     0.00001
   td         42     0.00003
   td       7681     0.00020
   td    9522739     0.14363
   td   19741889     4.29314
   td    5137920     4.40325
   td     546423     0.82931
   td      53310     0.09395
   td       5386     0.01127
   td        630     0.00140
   td         83     0.00021
   td         21     0.00004
   td          6     0.00001
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          9.89416
Average Time:        9.89416
OMP_NUM_THREADS	8
Read Time:           6.97210
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.04168
   td          4     0.00002
   td         42     0.00004
   td       7681     0.00017
   td    9522739     0.09815
   td   19741889     2.18358
   td    5137920     2.16460
   td     546423     0.44092
   td      53310     0.04918
   td       5386     0.00552
   td        630     0.00087
   td         83     0.00014
   td         21     0.00002
   td          6     0.00001
   td          2     0.00001
   td          1     0.00000
   td          0     0.00000
Trial Time:          5.02071
Average Time:        5.02071
OMP_NUM_THREADS	16
Read Time:           6.94632
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02576
   td          4     0.00002
   td         42     0.00010
   td       7681     0.00021
   td    9522739     0.06009
   td   19741889     1.23776
   td    5137920     1.38461
   td     546423     0.35260
   td      53310     0.03369
   td       5386     0.00412
   td        630     0.00069
   td         83     0.00010
   td         21     0.00002
   td          6     0.00001
   td          2     0.00001
   td          1     0.00001
   td          0     0.00000
Trial Time:          3.12775
Average Time:        3.12775
OMP_NUM_THREADS	24
Read Time:           6.97787
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02041
   td          4     0.00002
   td         42     0.00024
   td       7681     0.00019
   td    9522739     0.06842
   td   19741889     1.04896
   td    5137920     1.19999
   td     546423     0.29482
   td      53310     0.02989
   td       5386     0.00409
   td        630     0.00054
   td         83     0.00008
   td         21     0.00002
   td          6     0.00001
   td          2     0.00001
   td          1     0.00001
   td          0     0.00000
Trial Time:          2.69413
Average Time:        2.69413
OMP_NUM_THREADS	48
Read Time:           6.98423
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02033
   td          4     0.00002
   td         42     0.00092
   td       7681     0.00031
   td    9522739     0.08338
   td   19741889     1.30053
   td    5137920     1.69966
   td     546423     0.41142
   td      53310     0.04174
   td       5386     0.00611
   td        630     0.00071
   td         83     0.00011
   td         21     0.00004
   td          6     0.00004
   td          2     0.00003
   td          1     0.00001
   td          0     0.00001
Trial Time:          3.59264
Average Time:        3.59264
OMP_NUM_THREADS	96
Read Time:           7.22155
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02467
   td          4     0.00004
   td         42     0.00304
   td       7681     0.00035
   td    9522739     0.08236
   td   19741889     1.52285
   td    5137920     2.13335
   td     546423     0.58918
   td      53310     0.05615
   td       5386     0.00713
   td        630     0.00081
   td         83     0.00016
   td         21     0.00007
   td          6     0.00005
   td          2     0.00005
   td          1     0.00001
   td          0     0.00001
Trial Time:          4.44736
Average Time:        4.44736
OMP_NUM_THREADS	192
Read Time:           7.27443
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02164
   td          4     0.00005
   td         42     0.01421
   td       7681     0.00057
   td    9522739     0.14154
   td   19741889     2.17453
   td    5137920     1.96220
   td     546423     0.39757
   td      53310     0.04376
   td       5386     0.00525
   td        630     0.00065
   td         83     0.00015
   td         21     0.00008
   td          6     0.00007
   td          2     0.00007
   td          1     0.00002
   td          0     0.00001
Trial Time:          4.82224
Average Time:        4.82224
