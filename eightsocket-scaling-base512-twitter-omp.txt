OMP_NUM_THREADS	96
Read Time:           7.71181
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02495
   td          4     0.00005
   td         42     0.00012
   td       7681     0.00088
   td    9522739     0.21623
   td   19741889     0.75657
   td    5137920     0.29643
   td     546423     0.04154
   td      53310     0.00594
   td       5386     0.00064
   td        630     0.00015
   td         83     0.00011
   td         21     0.00003
   td          6     0.00002
   td          2     0.00001
   td          1     0.00001
   td          0     0.00001
Trial Time:          1.36998
Average Time:        1.36998
