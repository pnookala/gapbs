OMP_NUM_THREADS	96
Read Time:           8.54665
Graph has 61578415 nodes and 1468364884 directed edges for degree: 23
Source:             12441072
    i                0.02557
   td          4     0.00005
   td         42     0.00012
   td       7681     0.00085
   td    9522739     0.30162
   td   19741889     1.11519
   td    5137920     0.41790
   td     546423     0.03745
   td      53310     0.00494
   td       5386     0.00085
   td        630     0.00021
   td         83     0.00011
   td         21     0.00004
   td          6     0.00002
   td          2     0.00001
   td          1     0.00002
   td          0     0.00002
Trial Time:          1.93140
Average Time:        1.93140
