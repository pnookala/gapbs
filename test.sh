#!/bin/bash

num_threads=(1 2 4 8 16 24 48 96 192)
test_name=(gomp)
graph_name=(friendster twitter)
hostname=`awk '{print $1}' /etc/hostname`
cpus=$(nproc)

export OMP_PROC_BIND=close
export OMP_PLACES=cores

for g in "${graph_name[@]}"
	do
		rawfilename=${hostname}-scaling-${g}-${test_name}
		for t in "${num_threads[@]}"
    do
			echo "OMP_NUM_THREADS	${t}" >> ${rawfilename}.txt
			OMP_NUM_THREADS=${t} ./bfs -f benchmark/graphs/${g}.sg >> ${rawfilename}.txt
		done
	done 
